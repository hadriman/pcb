EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2050 2950 2050 3050
Wire Wire Line
	2050 2350 2050 2450
Wire Wire Line
	2550 2950 2550 3050
Wire Wire Line
	2550 2350 2550 2450
Wire Wire Line
	2700 2950 2700 3050
Wire Wire Line
	2700 2350 2700 2450
Wire Wire Line
	2050 2750 2550 2750
Wire Wire Line
	2050 2850 2550 2850
Wire Wire Line
	3200 2950 3200 3050
Wire Wire Line
	3200 2350 3200 2450
Wire Wire Line
	3400 2950 3400 3050
Wire Wire Line
	3400 2350 3400 2450
Wire Wire Line
	3900 2950 3900 3050
Wire Wire Line
	3900 2350 3900 2450
Wire Wire Line
	5400 2950 5400 3050
Wire Wire Line
	5400 2350 5400 2450
Wire Wire Line
	5900 2950 5900 3050
Wire Wire Line
	5900 2350 5900 2450
Wire Wire Line
	6050 2950 6050 3050
Wire Wire Line
	6050 2350 6050 2450
Wire Wire Line
	6550 2950 6550 3050
Wire Wire Line
	6550 2350 6550 2450
Wire Wire Line
	6700 2950 6700 3050
Wire Wire Line
	6700 2350 6700 2450
Wire Wire Line
	6050 2750 6550 2750
Wire Wire Line
	6050 2850 6550 2850
Wire Wire Line
	7200 2950 7200 3050
Wire Wire Line
	7200 2350 7200 2450
Wire Wire Line
	7400 2950 7400 3050
Wire Wire Line
	7400 2350 7400 2450
Wire Wire Line
	7900 2950 7900 3050
Wire Wire Line
	7900 2350 7900 2450
Wire Wire Line
	8050 2950 8050 3050
Wire Wire Line
	8050 2350 8050 2450
Wire Wire Line
	7400 2750 7900 2750
Wire Wire Line
	7400 2850 7900 2850
Wire Wire Line
	8550 2950 8550 3050
Wire Wire Line
	8550 2350 8550 2450
Wire Wire Line
	4050 2950 4050 3050
Wire Wire Line
	4050 2350 4050 2450
Wire Wire Line
	3400 2750 3900 2750
Wire Wire Line
	3400 2850 3900 2850
Wire Wire Line
	4550 2950 4550 3050
Wire Wire Line
	4550 2350 4550 2450
Wire Wire Line
	4750 2950 4750 3050
Wire Wire Line
	4750 2350 4750 2450
Wire Wire Line
	2050 2150 2550 2150
Wire Wire Line
	5250 2950 5250 3050
Wire Wire Line
	2050 3350 2550 3350
Wire Wire Line
	2050 3450 2550 3450
Wire Wire Line
	2050 2250 2550 2250
Wire Wire Line
	5250 2350 5250 2450
Wire Wire Line
	4750 2750 5250 2750
Wire Wire Line
	4750 2850 5250 2850
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J1
U 1 1 5BEFA65F
P 2250 2750
F 0 "J1" H 2300 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 2300 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 2250 2750 50  0001 C CNN
F 3 "~" H 2250 2750 50  0001 C CNN
	1    2250 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J2
U 1 1 5BEFB373
P 2900 2750
F 0 "J2" H 2950 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 2950 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 2900 2750 50  0001 C CNN
F 3 "~" H 2900 2750 50  0001 C CNN
	1    2900 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J3
U 1 1 5BEFC09B
P 3600 2750
F 0 "J3" H 3650 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 3650 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 3600 2750 50  0001 C CNN
F 3 "~" H 3600 2750 50  0001 C CNN
	1    3600 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J4
U 1 1 5BEFCDBB
P 4250 2750
F 0 "J4" H 4300 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 4300 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 4250 2750 50  0001 C CNN
F 3 "~" H 4250 2750 50  0001 C CNN
	1    4250 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J5
U 1 1 5BEFDAFF
P 4950 2750
F 0 "J5" H 5000 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 5000 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 4950 2750 50  0001 C CNN
F 3 "~" H 4950 2750 50  0001 C CNN
	1    4950 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J6
U 1 1 5BEFE819
P 5600 2750
F 0 "J6" H 5650 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 5650 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 5600 2750 50  0001 C CNN
F 3 "~" H 5600 2750 50  0001 C CNN
	1    5600 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J7
U 1 1 5BEFF543
P 6250 2750
F 0 "J7" H 6300 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 6300 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 6250 2750 50  0001 C CNN
F 3 "~" H 6250 2750 50  0001 C CNN
	1    6250 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J8
U 1 1 5BF00297
P 6900 2750
F 0 "J8" H 6950 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 6950 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 6900 2750 50  0001 C CNN
F 3 "~" H 6900 2750 50  0001 C CNN
	1    6900 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J9
U 1 1 5BF00FBC
P 7600 2750
F 0 "J9" H 7650 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 7650 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 7600 2750 50  0001 C CNN
F 3 "~" H 7600 2750 50  0001 C CNN
	1    7600 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J10
U 1 1 5BF01CE4
P 8250 2750
F 0 "J10" H 8300 3567 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 8300 3476 50  0000 C CNN
F 2 "raw_holes:2x14_P2.54mm_raw_PTH" H 8250 2750 50  0001 C CNN
F 3 "~" H 8250 2750 50  0001 C CNN
	1    8250 2750
	1    0    0    -1  
$EndComp
Connection ~ 2550 2150
Wire Wire Line
	2550 2150 2700 2150
Connection ~ 2550 2250
Wire Wire Line
	2550 2250 2700 2250
Connection ~ 2700 2150
Wire Wire Line
	2700 2150 3200 2150
Connection ~ 2700 2250
Wire Wire Line
	2700 2250 3200 2250
Connection ~ 3200 2150
Wire Wire Line
	3200 2150 3400 2150
Connection ~ 3200 2250
Wire Wire Line
	3200 2250 3400 2250
Connection ~ 3400 2150
Wire Wire Line
	3400 2150 3900 2150
Connection ~ 3400 2250
Wire Wire Line
	3400 2250 3900 2250
Connection ~ 3900 2150
Wire Wire Line
	3900 2150 4050 2150
Connection ~ 3900 2250
Wire Wire Line
	3900 2250 4050 2250
Connection ~ 4050 2150
Wire Wire Line
	4050 2150 4550 2150
Connection ~ 4050 2250
Wire Wire Line
	4050 2250 4550 2250
Connection ~ 4550 2150
Wire Wire Line
	4550 2150 4750 2150
Connection ~ 4550 2250
Wire Wire Line
	4550 2250 4750 2250
Connection ~ 4750 2150
Wire Wire Line
	4750 2150 5250 2150
Connection ~ 4750 2250
Wire Wire Line
	4750 2250 5250 2250
Connection ~ 5250 2150
Wire Wire Line
	5250 2150 5400 2150
Connection ~ 5250 2250
Wire Wire Line
	5250 2250 5400 2250
Connection ~ 5400 2150
Wire Wire Line
	5400 2150 5900 2150
Connection ~ 5400 2250
Wire Wire Line
	5400 2250 5900 2250
Connection ~ 5900 2150
Wire Wire Line
	5900 2150 6050 2150
Connection ~ 5900 2250
Wire Wire Line
	5900 2250 6050 2250
Connection ~ 6050 2150
Wire Wire Line
	6050 2150 6550 2150
Connection ~ 6050 2250
Wire Wire Line
	6050 2250 6550 2250
Connection ~ 6550 2150
Wire Wire Line
	6550 2150 6700 2150
Connection ~ 6550 2250
Wire Wire Line
	6550 2250 6700 2250
Connection ~ 6700 2150
Wire Wire Line
	6700 2150 7200 2150
Connection ~ 6700 2250
Wire Wire Line
	6700 2250 7200 2250
Connection ~ 7200 2150
Wire Wire Line
	7200 2150 7400 2150
Connection ~ 7200 2250
Wire Wire Line
	7200 2250 7400 2250
Connection ~ 7400 2150
Wire Wire Line
	7400 2150 7900 2150
Connection ~ 7400 2250
Wire Wire Line
	7400 2250 7900 2250
Connection ~ 7900 2150
Wire Wire Line
	7900 2150 8050 2150
Connection ~ 7900 2250
Wire Wire Line
	7900 2250 8050 2250
Connection ~ 8050 2150
Wire Wire Line
	8050 2150 8550 2150
Connection ~ 8050 2250
Connection ~ 2050 2450
Wire Wire Line
	2050 2450 2050 2550
Connection ~ 2050 2550
Wire Wire Line
	2050 2550 2050 2650
Connection ~ 2050 3050
Wire Wire Line
	2050 3050 2050 3150
Connection ~ 2050 3150
Wire Wire Line
	2050 3150 2050 3250
Connection ~ 2550 2450
Wire Wire Line
	2550 2450 2550 2550
Connection ~ 2550 2550
Wire Wire Line
	2550 2550 2550 2650
Connection ~ 2550 2750
Wire Wire Line
	2550 2750 2700 2750
Connection ~ 2550 2850
Wire Wire Line
	2550 2850 2700 2850
Connection ~ 2550 3050
Wire Wire Line
	2550 3050 2550 3150
Connection ~ 2550 3150
Wire Wire Line
	2550 3150 2550 3250
Connection ~ 2550 3350
Wire Wire Line
	2550 3350 2700 3350
Connection ~ 2550 3450
Wire Wire Line
	2550 3450 2700 3450
Connection ~ 2700 2450
Wire Wire Line
	2700 2450 2700 2550
Connection ~ 2700 2550
Wire Wire Line
	2700 2550 2700 2650
Connection ~ 2700 2750
Wire Wire Line
	2700 2750 3200 2750
Connection ~ 2700 2850
Wire Wire Line
	2700 2850 3200 2850
Connection ~ 2700 3050
Wire Wire Line
	2700 3050 2700 3150
Connection ~ 2700 3150
Wire Wire Line
	2700 3150 2700 3250
Connection ~ 2700 3350
Wire Wire Line
	2700 3350 3200 3350
Connection ~ 2700 3450
Wire Wire Line
	2700 3450 3200 3450
Connection ~ 3200 2450
Wire Wire Line
	3200 2450 3200 2550
Connection ~ 3200 2550
Wire Wire Line
	3200 2550 3200 2650
Connection ~ 3200 3050
Wire Wire Line
	3200 3050 3200 3150
Connection ~ 3200 3150
Wire Wire Line
	3200 3150 3200 3250
Connection ~ 3200 3350
Wire Wire Line
	3200 3350 3400 3350
Connection ~ 3200 3450
Wire Wire Line
	3200 3450 3400 3450
Connection ~ 3400 2450
Wire Wire Line
	3400 2450 3400 2550
Connection ~ 3400 2550
Wire Wire Line
	3400 2550 3400 2650
Connection ~ 3400 3050
Wire Wire Line
	3400 3050 3400 3150
Connection ~ 3400 3150
Wire Wire Line
	3400 3150 3400 3250
Connection ~ 3400 3350
Wire Wire Line
	3400 3350 3900 3350
Connection ~ 3400 3450
Wire Wire Line
	3400 3450 3900 3450
Connection ~ 3900 2450
Wire Wire Line
	3900 2450 3900 2550
Connection ~ 3900 2550
Wire Wire Line
	3900 2550 3900 2650
Connection ~ 3900 2750
Wire Wire Line
	3900 2750 4050 2750
Connection ~ 3900 2850
Wire Wire Line
	3900 2850 4050 2850
Connection ~ 3900 3050
Wire Wire Line
	3900 3050 3900 3150
Connection ~ 3900 3150
Wire Wire Line
	3900 3150 3900 3250
Connection ~ 3900 3350
Wire Wire Line
	3900 3350 4050 3350
Connection ~ 3900 3450
Wire Wire Line
	3900 3450 4050 3450
Connection ~ 4050 2450
Wire Wire Line
	4050 2450 4050 2550
Connection ~ 4050 2550
Wire Wire Line
	4050 2550 4050 2650
Connection ~ 4050 2750
Wire Wire Line
	4050 2750 4550 2750
Connection ~ 4050 2850
Wire Wire Line
	4050 2850 4550 2850
Connection ~ 4050 3050
Wire Wire Line
	4050 3050 4050 3150
Connection ~ 4050 3150
Wire Wire Line
	4050 3150 4050 3250
Connection ~ 4050 3350
Wire Wire Line
	4050 3350 4550 3350
Connection ~ 4050 3450
Wire Wire Line
	4050 3450 4550 3450
Connection ~ 4550 2450
Wire Wire Line
	4550 2450 4550 2550
Connection ~ 4550 2550
Wire Wire Line
	4550 2550 4550 2650
Connection ~ 4550 3050
Wire Wire Line
	4550 3050 4550 3150
Connection ~ 4550 3150
Wire Wire Line
	4550 3150 4550 3250
Connection ~ 4550 3350
Wire Wire Line
	4550 3350 4750 3350
Connection ~ 4550 3450
Wire Wire Line
	4550 3450 4750 3450
Connection ~ 4750 2450
Wire Wire Line
	4750 2450 4750 2550
Connection ~ 4750 2550
Wire Wire Line
	4750 2550 4750 2650
Connection ~ 4750 3050
Wire Wire Line
	4750 3050 4750 3150
Connection ~ 4750 3150
Wire Wire Line
	4750 3150 4750 3250
Connection ~ 4750 3350
Wire Wire Line
	4750 3350 5250 3350
Connection ~ 4750 3450
Wire Wire Line
	4750 3450 5250 3450
Connection ~ 5250 2450
Wire Wire Line
	5250 2450 5250 2550
Connection ~ 5250 2550
Wire Wire Line
	5250 2550 5250 2650
Connection ~ 5250 2750
Wire Wire Line
	5250 2750 5400 2750
Connection ~ 5250 2850
Wire Wire Line
	5250 2850 5400 2850
Connection ~ 5250 3050
Wire Wire Line
	5250 3050 5250 3150
Connection ~ 5250 3150
Wire Wire Line
	5250 3150 5250 3250
Connection ~ 5250 3350
Wire Wire Line
	5250 3350 5400 3350
Connection ~ 5250 3450
Wire Wire Line
	5250 3450 5400 3450
Connection ~ 5400 2450
Wire Wire Line
	5400 2450 5400 2550
Connection ~ 5400 2550
Wire Wire Line
	5400 2550 5400 2650
Connection ~ 5400 2750
Wire Wire Line
	5400 2750 5900 2750
Connection ~ 5400 2850
Wire Wire Line
	5400 2850 5900 2850
Connection ~ 5400 3050
Wire Wire Line
	5400 3050 5400 3150
Connection ~ 5400 3150
Wire Wire Line
	5400 3150 5400 3250
Connection ~ 5400 3350
Wire Wire Line
	5400 3350 5900 3350
Connection ~ 5400 3450
Wire Wire Line
	5400 3450 5900 3450
Connection ~ 5900 2450
Wire Wire Line
	5900 2450 5900 2550
Connection ~ 5900 2550
Wire Wire Line
	5900 2550 5900 2650
Connection ~ 5900 3050
Wire Wire Line
	5900 3050 5900 3150
Connection ~ 5900 3150
Wire Wire Line
	5900 3150 5900 3250
Connection ~ 5900 3350
Wire Wire Line
	5900 3350 6050 3350
Connection ~ 5900 3450
Wire Wire Line
	5900 3450 6050 3450
Connection ~ 6050 2450
Wire Wire Line
	6050 2450 6050 2550
Connection ~ 6050 2550
Wire Wire Line
	6050 2550 6050 2650
Connection ~ 6050 3050
Wire Wire Line
	6050 3050 6050 3150
Connection ~ 6050 3150
Wire Wire Line
	6050 3150 6050 3250
Connection ~ 6050 3350
Wire Wire Line
	6050 3350 6550 3350
Connection ~ 6050 3450
Wire Wire Line
	6050 3450 6550 3450
Connection ~ 6550 2450
Wire Wire Line
	6550 2450 6550 2550
Connection ~ 6550 2550
Wire Wire Line
	6550 2550 6550 2650
Connection ~ 6550 2750
Wire Wire Line
	6550 2750 6700 2750
Connection ~ 6550 2850
Wire Wire Line
	6550 2850 6700 2850
Connection ~ 6550 3050
Wire Wire Line
	6550 3050 6550 3150
Connection ~ 6550 3150
Wire Wire Line
	6550 3150 6550 3250
Connection ~ 6550 3350
Wire Wire Line
	6550 3350 6700 3350
Connection ~ 6550 3450
Wire Wire Line
	6550 3450 6700 3450
Connection ~ 6700 2450
Wire Wire Line
	6700 2450 6700 2550
Connection ~ 6700 2550
Wire Wire Line
	6700 2550 6700 2650
Connection ~ 6700 2750
Wire Wire Line
	6700 2750 7200 2750
Connection ~ 6700 2850
Wire Wire Line
	6700 2850 7200 2850
Connection ~ 6700 3050
Wire Wire Line
	6700 3050 6700 3150
Connection ~ 6700 3150
Wire Wire Line
	6700 3150 6700 3250
Connection ~ 6700 3350
Wire Wire Line
	6700 3350 7200 3350
Connection ~ 6700 3450
Wire Wire Line
	6700 3450 7200 3450
Connection ~ 7200 2450
Wire Wire Line
	7200 2450 7200 2550
Connection ~ 7200 2550
Wire Wire Line
	7200 2550 7200 2650
Connection ~ 7200 3050
Wire Wire Line
	7200 3050 7200 3150
Connection ~ 7200 3150
Wire Wire Line
	7200 3150 7200 3250
Connection ~ 7200 3350
Wire Wire Line
	7200 3350 7400 3350
Connection ~ 7200 3450
Wire Wire Line
	7200 3450 7400 3450
Connection ~ 7400 2450
Wire Wire Line
	7400 2450 7400 2550
Connection ~ 7400 2550
Wire Wire Line
	7400 2550 7400 2650
Connection ~ 7400 3050
Wire Wire Line
	7400 3050 7400 3150
Connection ~ 7400 3150
Wire Wire Line
	7400 3150 7400 3250
Connection ~ 7400 3350
Wire Wire Line
	7400 3350 7900 3350
Connection ~ 7400 3450
Wire Wire Line
	7400 3450 7900 3450
Connection ~ 7900 2450
Wire Wire Line
	7900 2450 7900 2550
Connection ~ 7900 2550
Wire Wire Line
	7900 2550 7900 2650
Connection ~ 7900 2750
Wire Wire Line
	7900 2750 8050 2750
Connection ~ 7900 2850
Wire Wire Line
	7900 2850 8050 2850
Connection ~ 7900 3050
Wire Wire Line
	7900 3050 7900 3150
Connection ~ 7900 3150
Wire Wire Line
	7900 3150 7900 3250
Connection ~ 7900 3350
Wire Wire Line
	7900 3350 8050 3350
Connection ~ 7900 3450
Wire Wire Line
	7900 3450 8050 3450
Connection ~ 8050 2450
Wire Wire Line
	8050 2450 8050 2550
Connection ~ 8050 2550
Wire Wire Line
	8050 2550 8050 2650
Connection ~ 8050 2750
Wire Wire Line
	8050 2750 8550 2750
Connection ~ 8050 2850
Wire Wire Line
	8050 2850 8550 2850
Connection ~ 8050 3050
Wire Wire Line
	8050 3050 8050 3150
Connection ~ 8050 3150
Wire Wire Line
	8050 3150 8050 3250
Connection ~ 8050 3350
Wire Wire Line
	8050 3350 8550 3350
Connection ~ 8050 3450
Wire Wire Line
	8050 3450 8550 3450
Connection ~ 8550 2450
Wire Wire Line
	8550 2450 8550 2550
Connection ~ 8550 2550
Wire Wire Line
	8550 2550 8550 2650
Connection ~ 8550 3050
Wire Wire Line
	8550 3050 8550 3150
Connection ~ 8550 3150
Wire Wire Line
	8550 3150 8550 3250
Wire Wire Line
	8050 2250 8550 2250
Connection ~ 8550 3350
Connection ~ 8550 2250
Connection ~ 8550 2150
Connection ~ 8550 3450
Wire Wire Line
	8550 2250 8750 2250
Wire Wire Line
	8550 2150 8950 2150
Wire Wire Line
	8550 3350 8750 3350
Wire Wire Line
	8550 3450 8950 3450
Wire Wire Line
	8750 2250 8750 3350
Wire Wire Line
	8950 2150 8950 3450
$EndSCHEMATC
