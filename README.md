# PCB

Collection of PCBs designed by me for my personal use

Warning : there may be a few files in this repo that I do not own, be aware of 
this and check carefully the respective licences of everything you take.

The rest of the stuff is mine and licensed under GNU GPLv3.